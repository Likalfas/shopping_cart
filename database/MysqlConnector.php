<?php
/**
 * Database Abstraction Class
 */
class MysqlConnector
{
    /**
     * Instance of the Singleton Design Pattern
     */
    private static $instance;
    /**
     * Instance of Mysqli Database
     */
    private $db;
    private $host = "localhost";
    private $username = "root";
    private $password = "";
    private $dbname = "shopping_cart";

    /**
     * Db constructor.
     */
    public function __construct()
    {
        $this->db = @new mysqli( $this->host, $this->username, $this->password, $this->dbname );
        if( $this->db->connect_errno )
        {
            die('Unable to connect to database [' . $this->db->connect_error . ']');
        }
    }

    public function __destruct()
    {
        $this->db->close();
    }

    /**
     * Returns the instance of the singleton
     * @return mixed
     */
    public static function getInstance()
    {
        if( ! isset( MysqlConnector::$instance ) )
        {
            MysqlConnector::$instance = new MysqlConnector();
        }
        return MysqlConnector::$instance;
    }

    /**
     * Inserts data into database
     * @param $table
     * @param array $data
     * @return bool
     */
    public function insert($table, $data = array() )
    {
        if( count($data) )
        {
            $keys = array_keys( $data[0] );
            $cols = implode( ', ', $keys );
            $query = 'INSERT INTO ' . $table . '(' . $cols . ') VALUES ';

            foreach( $data as $k => $row )
            {
                $query .= $k > 0 ? ',(' : '(';
                foreach( $row as $key => $value )
                {
                    $query .= ($key != $keys[0] ? ',' : '');
                    $query .= (is_numeric($value) ? $value : '\'' . $value . '\'');
                }
                $query .= ')';
            }

            if( ! $res = $this->db->query( $query ) )
            {
                die( 'Error running query [' . $this->db->error . '] \n [' . $query . ']' );
            }
            return true;
        }
        else
        {
            die( "Unable to insert data, no data provided" );
        }
    }

    /**
     * Selects data in database
     * @param $cols
     * @param $table
     * @param null $where
     * @param null $limit
     * @return array
     */
    public function select($cols, $table, $where = null, $limit = null )
    {
        $where = empty( $where ) ? '' : " WHERE $where";
        $limit = empty( $limit ) ? '' : " LIMIT $limit";
        $query = 'SELECT ' . $cols . ' FROM ' . $table . $where . $limit;

        $ret = $this->fetch_assoc( $query );
        if( !empty($ret) )
        {
            return ($limit == " LIMIT 1" ? $ret[0] : $ret);
        }
        return false;
    }

    /**
     * Updates data in database
     * @param $table
     * @param array $data
     * @param null $where
     */
    public function update($table, $data = array(), $where = null )
    {
        if( count($data) )
        {
            $query = 'UPDATE ' . $table . ' SET ';
            foreach( $data as $key => $value )
            {
                $query .= $key . '=' . (is_string($value) ? '\'' . $value . '\'' : $value) . ',';
            }
            $query = substr( $query, 0, -1 );
            $query .= empty( $where ) ? '' : ' WHERE ' . $where;
            $res = $this->db->query( $query );
            if( !$res || !$this->db->affected_rows )
            {
                die( 'Error running query => [' . $this->db->error . ']
                      <br>Query => [' . $query . ']
                      <br>Affected Rows => [' . $this->db->affected_rows . ']' );
            }
            return TRUE;
        }
        else
        {
            die( "Unable to update data, no data provided" );
        }
    }

    /**
     * Deletes data from database
     * @param $table
     * @param null $where
     */
    public function delete($table, $where = null )
    {
        $query = 'DELETE FROM ' . $table . (empty( $where ) ? '' : ' WHERE ' . $where);
        $res = $this->db->query( $query );

        if( !$res || !$this->db->affected_rows )
        {
            die( 'Error running query => [' . $this->db->error . ']
                      <br>Query => [' . $query . ']
                      <br>Affected Rows => [' . $this->db->affected_rows . ']' );
        }
        return TRUE;
    }

    /**
     * Function that will serve any type of query,
     * mainly from more complicated queries
     * @param $query
     * @return array
     */
    public function query($query )
    {
        return $this->fetch_assoc( $query );
    }

    /**
     * Fetches array of data from query
     * @param $query
     * @return array
     */
    public function fetch_assoc($query )
    {
        $arr = array();
        if( ! $res = $this->db->query( $query ) )
        {
            die( 'Error running query [' . $this->db->error . '] \n [' . $query . ']' );
        }

        while( $row = $res->fetch_assoc() )
        {
            $temp = array();
            foreach( $row as $key => $value )
            {
                $temp[$key] = $value;
            }
            array_push( $arr, $temp );
        }
        return $arr;
    }
}