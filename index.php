<?php
// Ouvre la session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once("controller/ArticleController.php");
require_once("controller/CartController.php");
// Instruction utilisée pour débugger
//unset($_SESSION["cart"]);

/* Lorsqu'on ajoute ou enlève un produit du panier,
   on soumet un formulaire avec des données envoyé en POST */
if(isset($_POST["action"]) && isset($_POST["article_id"]))
{
  $controller = new CartController();
  $action = $_POST["action"]; // Action : Ajouter / Enlever
  $article_id = $_POST["article_id"];
  
  switch($action)
  {
    case "add":
      $controller->add($article_id);
      $controller->showDetail();
      break;
    case "remove":
      $controller->remove($article_id);
      $controller->showDetail();
      break;
    default:
      // La page par défaut est la liste des articles
      $controller = new ArticleController();
      $controller->showList();
      break;
  }
}else if(isset($_GET["action"]) && $_GET["action"] == "cart")
{
    // Si on souhaite se rendre sur la page du panier
    $controller = new CartController();
    $controller->showDetail();
}else
{
  // La page par défaut est la liste des articles
  $controller = new ArticleController();
  $controller->showList();
}


