    <div class="page">
      <h1>
        Liste d'articles
      </h1>
      <table>
        <tr>
          <th>Nom</th>
          <th>Description</th>
          <th>Prix</th>
          <th>Action</th>
        </tr>
        <?php foreach($articles as $key => $article){ ?>
        <tr>
          <td><?= $article["name"] ?></td>
          <td><?= $article["description"] ?></td>
          <td><?= $article["price"] ?> &euro;</td>
          <td>
            <form action="/" method="post">
              <input type="hidden" name="article_id" value="<?= $article["id"] ?>" />
              <input type="hidden" name="article_name" value="<?= $article["name"] ?>" />
              <input type="hidden" name="article_desc" value="<?= $article["description"] ?>" />
              <input type="hidden" name="action" value="add" />
              <input type="submit" value="Ajouter au panier"/>
            </form>
          </td>
        </tr>
        <?php } ?>
      </table>
    </div>
