
  <div class="cart page">
    <h1>
      Panier
    </h1>
      <table>
        <tr>
          <th>Nom</th>
          <th>Description</th>
          <th>Prix</th>
          <th>Quantité</th>
        </tr>
        <?php foreach($cart as $key => $article){ ?>
        <tr>
          <td><?= $article["name"] ?></td>
          <td><?= $article["description"] ?></td>
          <td><?= $article["price"] ?> &euro;</td>
          <td>
            <form action="/" method="post">
              <input type="hidden" name="article_id" value="<?= $article["article_id"] ?>" />
              <input type="hidden" name="action" value="remove" />
              <input type="submit" value="-"/>
            </form>
            <?= $article["quantity"] ?>
            <form action="/" method="post">
              <input type="hidden" name="article_id" value="<?= $article["article_id"] ?>" />
              <input type="hidden" name="action" value="add" />
              <input type="submit" value="+"/>
            </form>
          </td>
        </tr>
        <?php } ?>
      </table>
    <div class="align-right">
      <b>Total : </b><?= $total ?> &euro;
    </div>
    </div>
