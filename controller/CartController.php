<?php
require_once("model/Cart.php");

class CartController
{
  private $model;
  
  public function __construct()
  {
    $this->model = new Cart();
  }
  
  /* Ajoute un produit au panier */
  public function add($article_id)
  {
    // Détermine le nouveau ID du panier
    $cart_id = $this->model->getNewCartId();
    if(!isset($_SESSION["cart"]) && $cart_id)
    {
      $_SESSION["cart"] = $cart_id;
    }
    $this->model->add($article_id, $_SESSION["cart"]);
  }
  
  /* Enleve un produit du panier */
  public function remove($article_id)
  {
    if(!isset($_SESSION["cart"]))
    {
      die("No session found.");
    }
    $this->model->remove($article_id, $_SESSION["cart"]);
    
    // Si la suppression d'un produit a vidé le panier on supprime la variable session
    if(isset($_SESSION["cart"]) && !$this->model->exists($_SESSION["cart"]))
    {
      unset($_SESSION["cart"]);
    }
  }
  
  /* Affiche la page du panier */
  public function showDetail()
  {
    /* Dans le cas ou le panier n'existe pas mais que la variable 
       session existe toujours, on l'efface */
    if(isset($_SESSION["cart"]) && !$this->model->exists($_SESSION["cart"]))
    {
      unset($_SESSION["cart"]);
    }
    
    // Si le panier est vide on affiche la vue correspondante
    if(!isset($_SESSION["cart"]))
    {
       include("view/header.php");
       include("view/cart-empty.php");
       include("view/footer.php");
    }else
    {
      extract(array("cart" => $this->model->getCartArticles($_SESSION["cart"]), 
                    "total" => $this->model->getTotal($_SESSION["cart"])
                   ));
      include("view/header.php");
      include("view/cart-detail.php");
      include("view/footer.php");
    }
  }
}