<?php
require_once("model/Article.php");

class ArticleController 
{
  private $model;
  
  public function __construct()
  {
    $this->model = new Article();
  }
  
  /* Affiche la page "Liste d'articles" */
  public function showList()
  {
    $articles = $this->model->getAll();
    extract(array("articles" => $articles));
    include("view/header.php");
    include("view/article-list.php");
    include("view/footer.php");
  }
}