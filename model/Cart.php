<?php
require_once("database/MysqlConnector.php");

class Cart 
{
  private $database;
  
  public function __construct()
  {
    $this->database = MysqlConnector::getInstance();
  }
  
  /* Ajoute un produit au panier. Soit:
   - Le produit existe deja. On augmente donc sa quantité
   - Le produit n'existe pas. On l'ajoute.
   */
  public function add($article_id, $cart_id)
  {
    if(!isset($article_id) || !isset($cart_id))
    {
      return FALSE;
    }
    /* On récupère la quantité du produit pour déterminer quelle action prendre:
     - Soit on met à jour les données dans la base de données
     - Soit on insère une nouvelle ligne
    */
    $res = $this->getQuantity($article_id, $cart_id);
    $quantity = $res["quantity"];
    
    if($quantity)
    {
      $quantity++;
      $res = $this->database->update("cart_article", array("quantity" => $quantity), "cart_id = '$cart_id' AND article_id = '$article_id'");
    }else{
      $res =  $this->database->insert('cart_article', array(
        array("cart_id" => $cart_id, "article_id" => $article_id, "quantity" => 1)
      ));
    }
    return $res;
  }
  
  /* Supprime un produit du panier. Soit:
   - La quantité du produit est supérieur à 1. On déduit la quantité de 1.
   - La quantité du produit est égale à 1. On supprime le produit du panier.
   */
  public function remove($article_id, $cart_id)
  {
    if(!isset($article_id) || !isset($cart_id))
    {
      return FALSE;
    }
    $res = $this->getQuantity($article_id, $cart_id);
    $quantity = $res["quantity"];
    
    if(!$quantity || $quantity < 1)
    {
      return FALSE;
    }
    
    if($quantity > 1)
    {
      $res = $this->database->update("cart_article", array("quantity" => $quantity-1), "cart_id = '$cart_id' AND article_id = '$article_id'");
    }else
    {
      $res = $this->database->delete("cart_article", "cart_id = '$cart_id' AND article_id = '$article_id'");
    }
    
    if($res)
    {
      return TRUE;
    }
    return FALSE;
  }
  
  /* Récupère la nouvelle ID du panier */
  public function getNewCartId()
  {
    $res = $this->database->select("MAX(cart_id) AS id", "cart_article", null, 1);
    if(count($res))
    {
      return $res["id"]+1;
    }
    return FALSE;
  }
  
  /* Récupère la quantité d'un produit dans un panier */
  public function getQuantity($article_id, $cart_id)
  {
    return $this->database->select("quantity", "cart_article", "cart_id = '$cart_id' AND article_id = '$article_id'", 1);
  }
  
  /* Récupère tous les articles d'un panier */
  public function getCartArticles($cart_id)
  {
    $articles = $this->database->select('*', 'article, cart_article', "article.id = cart_article.article_id AND cart_article.cart_id = '$cart_id'");
    return $articles;
  }
  
  /* Récupère le total de tous les articles d'un panier */
  public function getTotal($cart_id)
  {
    $res = $this->database->select('SUM(price * quantity) AS total', 'article, cart_article', "article.id = cart_article.article_id AND cart_article.cart_id = '$cart_id'", 1);
    return $res["total"];
  }
  
  /* Détermine si le panier existe dans la base de données */
  public function exists($cart_id)
  {
    if(!isset($cart_id))
    {
      return FALSE;
    }
    $res = $this->database->select('*', "cart_article", "cart_id = '$cart_id'");
    return $res ? TRUE : FALSE;
  }
}