<?php
require_once("database/MysqlConnector.php");

class Article 
{
  private $database;
  
  public function __construct()
  {
    $this->database = MysqlConnector::getInstance();
  }
  
  /* Récupère toutes les articles */
  public function getAll()
  {
    $article = $this->database->select('*', 'article');
    return $article;
  }
}