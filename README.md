# README #

Ceci est un projet de panier E-Commerce réalisé par Oliver Spicer. 
( oli.spicer@gmail.com )

Le projet a été réalisé avec la structure MVC ( Model / View / Controller ) 
afin de garantir une meillieure maintenabilité et compréhension du code.

Cependant il manque quelques éléments normalement trouvés dans ce type de 
projet. Comme par exemple un Routeur, chargé de diriger les appels HTTP 
vers les bonnes méthodes et controlleurs, ou la configuration d'un fichier 
.htaccess.

Ceci est dû à la nature du projet: Réaliser un panier en php sans framework
à rendre en moins d'une semaine.

Temps passé sur le projet:
1 journée (9h -> 18h) + une soirée (18h - 22h) avec des pauses réguliers.

